package main

import "fmt"

type ContaCorrente struct {
	titular 		string
	numeroAgencia	int
	numeroConta		int
	saldo 			float64
}

func (c *ContaCorrente) Sacar(valorDoSaque float64) string { 
	podeSacar := valorDoSaque > 0 && valorDoSaque <= c.saldo
	if podeSacar {
		c.saldo -= valorDoSaque
		return "Saque realizado com suceso!"
	}else {
		return "Saldo insuficiente!"
	}

}

func (c *ContaCorrente) Depositar(valorDeposito float64) (string, float64) { 
	podeDepositar := valorDeposito > 0
	if podeDepositar {
		c.saldo += valorDeposito
		return "Deposito realizado com suceso!", c.saldo
	}else {
		return "O valor de depósito é inválido!", valorDeposito
	}

}

func (c *ContaCorrente) Transferir(valorDaTransferencia float64, contaDestino *ContaCorrente) string {
	if valorDaTransferencia <= c.saldo && valorDaTransferencia > 0 {
		c.saldo -= valorDaTransferencia
		contaDestino.Depositar(valorDaTransferencia)
		return "Transferência realziada!"
	}else{
		return "Não foi possivel realizar a transferência!"
	}
}

func main() {
	contaDoGuilherme := ContaCorrente{titular: "Guilherme", saldo: 300}
	contaDaSilvia := ContaCorrente{titular: "Silvia", saldo: 400}

	status := contaDaSilvia.Transferir(200, &contaDoGuilherme)

	fmt.Println(status)
	fmt.Println(contaDaSilvia)
	fmt.Println(contaDoGuilherme)
	

}