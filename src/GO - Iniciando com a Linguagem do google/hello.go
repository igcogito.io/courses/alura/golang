package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const monitoramento = 3
const delay = 5

func main() {

	exibeIntroducao()

	for {
		exibeMenu()
		comando := leComando()
		switch comando {
		case 1:
			iniciarMonitoramento()
		case 2:
			exibirLogs()
		case 0:
			sairDoPrograma()
		default:
			opcaoInvalida()
		}
	}

}

func exibeIntroducao() {
	nome := "Igor"
	versao := 1.1
	fmt.Println("Olá sr.", nome)
	fmt.Println("a versão desse programa é a", versao)
}

func exibeMenu() {
	fmt.Println("1 - Iniciar monitoramento")
	fmt.Println("2 - Exibir Logs")
	fmt.Println("0 - sair do programa")
}

func leComando() int {
	var comandoLido int
	fmt.Scan(&comandoLido)
	fmt.Println("O comando escolhido foi o", comandoLido)
	fmt.Println("")

	return comandoLido
}

func exibirLogs() {
	fmt.Println("Exibindo Logs...")
	imprimeLogs()
}

func sairDoPrograma() {
	fmt.Println("Saindo do programa")
	os.Exit(0)
}

func opcaoInvalida() {
	fmt.Println("Não reconheço esse comando!")
	os.Exit(-1)
}

func iniciarMonitoramento() {
	fmt.Println("Monitorando....")
	fmt.Println("..........")
	//sites := []string{"https://www.alura.com.br/", "https://www.aws.training/", "https://web.dio.me/sign-in", "https://www.cursoemvideo.com/minha-conta/"}

	sites := LeSitesDoArquivo()

	for i := 0; i < monitoramento; i++ {
		for i, site := range sites {
			fmt.Println("Testando site:", i, ":", site)
			testaSite(site)
		}
		time.Sleep(delay * time.Second)
		fmt.Println("")
	}

	fmt.Println("")
}

func testaSite(site string) {
	resp, err := http.Get(site)

	if err != nil {
		fmt.Println("Ocorreu um erro:", err)
	}

	if resp.StatusCode == 200 {
		fmt.Println("Site", site, "carregado com sucesso!!")
		registraLog(site, true)
	} else {
		fmt.Println("Site", site, "fora do ar, Status code:", resp.StatusCode)
		registraLog(site, false)
	}
}

func LeSitesDoArquivo() []string {

	var sites []string
	arquivo, err := os.Open("sites.txt")

	if err != nil {
		fmt.Print("Ocorreu um erro:", err)
	}

	leitor := bufio.NewReader(arquivo)

	for {
		linha, err := leitor.ReadString('\n')
		linha = strings.TrimSpace(linha)

		sites = append(sites, linha)

		if err == io.EOF {
			break
		}
	}

	arquivo.Close()
	return sites
}

func registraLog(site string, status bool) {

	arquivo, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		fmt.Println(err)
	}

	arquivo.WriteString(time.Now().Format("02/01/2006 15:04:05") + " - " + site + " - online: " + strconv.FormatBool(status) + "\n")

	arquivo.Close()
}

func imprimeLogs() {

	arquivo, err := ioutil.ReadFile("log.txt")

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(arquivo))
}
